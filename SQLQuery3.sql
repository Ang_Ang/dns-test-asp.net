﻿CREATE TABLE Trees (
    [ID]       INT     PRIMARY KEY     IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (40) NOT NULL,
    [Parent]   VARCHAR (11) NOT NULL,
    [TypeItem] VARCHAR (7)  NOT NULL
);

INSERT INTO Trees (Name, Parent, TypeItem) 
	VALUES ('Root','#','root'),('Folder','1','folder'),('Folder_2','1','folder'),('file','8','leaf'),('Folder_3','1','folder'),('Folder_2_1','3','folder'),('Folder_2_2','3','folder'),('file_2','2','leaf'),('file_3','5','leaf'),('file','5','leaf');
