﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MyTree.Models
{
    public class Tree
    {
        public int id { get; set; }
        public string text { get; set; }
        public string parent { get; set; }
        public string type { get; set; }
    }
    public class TreeDBContext : DbContext
    {
        public DbSet<Tree> Trees { get; set; }
    }
}