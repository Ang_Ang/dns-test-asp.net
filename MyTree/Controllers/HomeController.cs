﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyTree.Models;


namespace MyTree.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        TreeDBContext db = new TreeDBContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTree()
        {
            List<Tree> Trees = db.Trees.ToList<Tree>();
            JsonResult JsonRes = Json(Trees, JsonRequestBehavior.AllowGet);
            return JsonRes;
        }

        public ActionResult ChangeTree(int id, string parent)
        {
            var item = db.Trees.First(a => a.id == id);
            item.parent = parent;
            db.SaveChanges();
            return null;
        }
    }
}